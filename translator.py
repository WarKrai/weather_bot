import pymysql
from pymysql.cursors import DictCursor
from contextlib import closing
from googletrans import Translator
import traceback

translated_array = []

class Translated_data:
    def __init__(self, id, translation):
        self.id = id
        self.translation = translation
        print('{} created'.format(self))

    def add_to_db(self):
        with connection.cursor() as cursor:
            query = 'UPDATE cities SET name_translation=(%s) WHERE id=(%s)'
            cursor.execute(query, [self.translation, self.id])
            print('{} added'.format(self))


def translate(word):
    translator = Translator(service_urls=['translate.google.com'])
    translation = translator.translate(word, dest="ru")
    return translation


connection = pymysql.connect(
    host='localhost',
    user='root',
    password='',
    db='weather_bot',
    charset='utf8mb4',
    cursorclass=DictCursor
)

with connection.cursor() as cursor:
    query = 'SELECT * FROM cities'
    cursor.execute(query)
    for res in cursor:
        try:
            translation = translate(res['name'])
            data = Translated_data(res['id'], translation)
            translated_array.append(data)
        except Exception as e:
            print('Ошибка!!!!!!!\n', traceback.format_exc())
            continue


connection.close()