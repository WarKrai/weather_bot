#-*- coding: utf-8 -*-
import telebot
import requests
import json
import pymysql
from pymysql.cursors import DictCursor
from contextlib import closing

################################# Conection to MySQL ############################################
connection = pymysql.connect(
    host='localhost',
    user='root',
    password='',
    db='weather_bot',
    charset='utf8mb4',
    cursorclass=DictCursor
)

################################ OpenWeatherMap.org api config ##################################
api = "3043f48c72f6b0d3e26843354bae9eb2"
url = "http://api.openweathermap.org/data/2.5/weather?"

################################ Telegram Bot Api Config #########################################
bot = telebot.TeleBot('1226346417:AAHDTidcAKLN_TfHWoTS9h1HQ_cKICJjFuw')


# check existing of city in the db
def city_exist(city):
    with connection.cursor() as cursor:
        query = 'SELECT COUNT(*) FROM cities WHERE translate=(%s) OR name=(%s)'
        cursor.execute(query, [city, city])
        for res in cursor:
            count = res['COUNT(*)']
        if count == 0:
            return False
        else:
            return True


# Convert Kelvin to Celsius
def kelvinToCels(temp):
    return round((float(temp) - 273.15), 2)


# get weather data as string from OpenWeatherMap.org
def get_weather_data(city):
    city_name = ''
    city_translate = ''
    city_country = ''
    with connection.cursor() as cursor:
        query = 'SELECT * FROM cities WHERE translate=(%s) OR name=(%s)'
        cursor.execute(query, [city, city])
        for res in cursor:
            city_name = res['name']
            city_translate = res['translate']
            city_country = res['country']
    answer = url + 'appid=' + api + '&q=' + city_name
    response = requests.get(answer)
    res = response.json()
    main = res['main']
    temp = str(kelvinToCels(main['temp']))
    pressure = str(main['pressure'])
    humidity = str(main['humidity'])
    description = str(res['weather'][0]['description'])
    result_str = 'Погода в городе ' + city_translate + ', ' + city_country + ':\n' + 'Температура: ' + temp + '˚C\n' \
    'Давление: ' + pressure + '\nОсадки: ' + humidity + '\n' + description + '.'
    return result_str


# start command
@bot.message_handler(commands=['start'])
def start_message(message):
    bot.send_message(message.chat.id, 'Данный бот показывает погоду в городах\n'
                                      'Посмотреть список городов /cities')


# list of cities command
@bot.message_handler(commands=['cities'])
def show_cities(message):
    res = 'Вот список всех городов: \n'
    with connection.cursor() as cursor:
        query = 'SELECT * FROM cities'
        cursor.execute(query)
        for city in cursor:
            city_str = city['name'] + ', ' + city['country'] + '\n'
            res.join(city_str)
    bot.send_message(message.chat.id, res)



# showing weather
@bot.message_handler(content_types=['text'])
def show_weather(message):
    if not city_exist(message.text):
        bot.send_message(message.chat.id, ' Город не найден, возможно его еще нет в нашей базе, или вы ввели'
                                          ' неправильное название. Попробуйте еще раз')
    else:
        bot.send_message(message.chat.id, get_weather_data(message.text))


bot.polling()
connection.close()
