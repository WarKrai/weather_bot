import pymysql
from pymysql.cursors import DictCursor

connection = pymysql.connect(
    host='localhost',
    user='root',
    password='',
    db='weather_bot',
    charset='utf8mb4',
    cursorclass=DictCursor
)

with connection.cursor() as cursor:
    query = 'SELECT name FROM cities WHERE name="Odessa"'
    cursor.execute(query)
    for res in cursor: print(res)

connection.close()
