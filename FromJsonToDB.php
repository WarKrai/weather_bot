<?php

$host = "localhost";
$database = "weather_bot";
$user = "root";
$password = "";

$pdo = new PDO('mysql:host=' . $host . ';dbname='. $database . ';charset=utf8', $user, $password);
$pdo->exec("SET NAMES utf8");

$json = file_get_contents("city.list.json");
$cities = json_decode($json, true);

foreach ($cities as $city) {
    $query = $pdo->prepare("INSERT INTO cities VALUES (:id, :name, :state, :country)");
    $query->execute(array(":id"=>$city['id'], ":name"=>$city['name'], ":state"=>$city['state'],
        ":country"=>$city['country']));
}

